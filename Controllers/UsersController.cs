﻿using System;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ControlPanel.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace ControlPanel.Controllers
{
    public class UsersController : Controller
    {
        public async Task<ActionResult> Index()
        {
            using (var client = new HttpClient())
            {
                using (var response = await client.GetAsync(MyConfig.URL_API_BASE + MyConfig.USERS))
                {
                    if (response != null && response.IsSuccessStatusCode)
                    {
                        var ProdutoJsonString = await response.Content.ReadAsStringAsync();
                        var users = JsonConvert.DeserializeObject<User[]>(ProdutoJsonString).ToList();

                        return View(users);
                    }
                    else
                    {
                        return View(null);
                    }
                }
            }     
        }

        public ActionResult Create()
        {
            return View();
        }

        public async Task<ActionResult> CreateUser(User user)
        {
            using (var client = new HttpClient())
            {
                var serializedProduto = JsonConvert.SerializeObject(user);
                var content = new StringContent(serializedProduto, Encoding.UTF8, "application/json");
                var result = await client.PostAsync(MyConfig.URL_API_BASE + MyConfig.USERS, content);

                return RedirectToAction("Index", "Users");
            }

        }

        [HttpPost]
        public async Task<bool> Delete(int id)
        {
            try
            {            
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(MyConfig.URL_API_BASE);
                    HttpResponseMessage responseMessage = await client.
                        DeleteAsync(String.Format("{0}/{1}", MyConfig.URL_API_BASE + MyConfig.USERS, id));

                    if (responseMessage.IsSuccessStatusCode)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }

            }
            catch (System.Exception)
            {
                return false;
            }

        }

        public async Task<ActionResult> Update(int id)
        {
            using (var client = new HttpClient())
            {
                using (var response = await client.GetAsync(MyConfig.URL_API_BASE + MyConfig.USERS + "/" + id))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        var ProdutoJsonString = await response.Content.ReadAsStringAsync();
                        var user = JsonConvert.DeserializeObject<User>(ProdutoJsonString);

                        return View(user);
                    }
                    else
                    {
                        return View(null);
                    }
                }
            }
        }

        [HttpPost]
        public async Task<ActionResult> UpdateUser(User user)
        {
            using (var client = new HttpClient())
            {
                HttpResponseMessage responseMessage = await 
                    client.PutAsJsonAsync(MyConfig.URL_API_BASE + MyConfig.USERS + "/" + user.Id, user);

                if (responseMessage.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index", "Users");
                }
                else
                {
                    return RedirectToAction("Index", "Users");
                }
            }
        }
    }
}