﻿using ControlPanel.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Net.Http;

namespace ControlPanel.Controllers
{
    public class AccountController : Controller
    {
        public IActionResult Login()
        {
            return View();
        }

        public async System.Threading.Tasks.Task<ActionResult> Validate(Admin admin)
        {
            if (admin.Email == null)
                return Json(new { status = false, message = "Email Inválido!" });

            using (var client = new HttpClient())
            {
                using (var response = await client.GetAsync(MyConfig.URL_API_BASE + MyConfig.ADMINS + "/" + admin.Email))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        var ProdutoJsonString = await response.Content.ReadAsStringAsync();
                        var adm = JsonConvert.DeserializeObject<Admin>(ProdutoJsonString);
                       
                        if (adm != null)
                        {
                            if (admin.Password.Equals(admin.Password)) {
                                return Json(new { status = true, message = "Usuário Válidado com Sucesso!" });
                            }
                            else
                            {
                                return Json(new { status = false, message = "Senha Inválida!" });
                            }
                        }
                        else
                        {
                            return Json(new { status = false, message = "Usuário Inválido!" });
                        }
                    }
                    else
                    {
                        return Json(new { status = false, message = "Ocorreu um erro! Tente novamente mais tarde!" });
                    }
                }
            }       
        }
    }
}