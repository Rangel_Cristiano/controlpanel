﻿using System;

namespace ControlPanel.Models
{
    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Phone { get; set; }
        public string Specialist { get; set; }
        public DateTime Created { get; set; }
    }
}
