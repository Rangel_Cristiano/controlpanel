﻿using System;

namespace ControlPanel.Models
{
    public class Admin
    {
        public Admin() { }

        public String Email { get; set; }

        public String Password { get; set; }
    }
}
